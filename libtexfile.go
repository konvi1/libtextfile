package libtextfile

import (
	"fmt"
	"io/ioutil"
)

/*
Написать библиотеку, которая предоставляет ряд методов
получение количества строк в текстовом файле
получение количества слов в текстовом файле
В методах необходимо предусмотреть возврат ошибок, которые могут произойти при чтении файла.

Для того, чтобы использовать эту библиотеку, в GitLab необходимо будет создать тэг.
*/

func CountLineInFile(filename string) (int, error) {
	// читаем данные из файла
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("error read file:", filename)
		return 0, err
	}
	countLine := 0

	fmt.Println(data)
	return countLine, nil
}
